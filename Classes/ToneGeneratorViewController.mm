 //
//  ToneGeneratorViewController.m
//  VowelSynth
//
//  Ben Guo 2011
//
//  boilerplate code for audio initialization from Matt Gallagher's tutorial
//  http://cocoawithlove.com/2010/10/ios-tone-generator-introduction-to.html
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//

#import "ToneGeneratorViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#include <iostream>
#include <cmath>
#include <time.h>
#include <stdlib.h>
#import "AccelerometerFilter.h"

#define kUpdateFrequency	60.0
#define kLocalizedPause		NSLocalizedString(@"Pause","pause taking samples")
#define kLocalizedResume	NSLocalizedString(@"Resume","resume taking samples")

using namespace std;

// initialize FIR filter objects
BQFilter LPFilter, F1Filter, F2Filter, F3Filter, F4Filter, LPFilter_2, F1Filter_2, F2Filter_2, F3Filter_2, F4Filter_2;

@interface ToneGeneratorViewController()

// Sets up a new accelerometer filter. 
-(void)changeFilter:(Class)filterClass;

@end

// audio render procedure: don't allocate memory, don't take any locks, don't waste time
OSStatus RenderTone(
	void *inRefCon, 
	AudioUnitRenderActionFlags 	*ioActionFlags, 
	const AudioTimeStamp 		*inTimeStamp, 
	UInt32 						inBusNumber, 
	UInt32 						inNumberFrames, 
	AudioBufferList 			*ioData)

{
	float sample = 0;
    float sample2 = 0;
    
	// Get some parameters out of the view controller
	ToneGeneratorViewController *viewController =
		(ToneGeneratorViewController *)inRefCon;
	double theta = viewController->theta;                       // osc 1 phase. modulate for vibrato?
    double theta2 = viewController->theta2;                     // osc 2 phase.
    double amplitude = viewController->amplitude;               // osc 1, osc 2 amplitude
    bool isBeingTouched = viewController->isBeingTouched;       // TRUE if screen is being touched
    bool isBeingTouched2 = viewController->isBeingTouched2;     // TRUE if screen is being touched twice
    double noiselevel = viewController->noiselevel;             // noise amplitude
    int waveform = viewController->waveform;                    // osc 1 + 2 waveform type
    double sampleRate = viewController->sampleRate;             
int envelope, envelope2;                                        // envelope to ramp signal on/off
	double theta_increment = 2.0 * M_PI * viewController->f0/sampleRate;    // osc 1 phase increment
    double theta_increment2 = 2.0 * M_PI * viewController->f0_2/sampleRate; // osc 2 phase increment

	// This is in mono so we only need the first buffer
	const int channel = 0;
	Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
	
	// Generate the samples
	for (UInt32 frame = 0; frame < inNumberFrames; frame++) 
	{
        // apply on/off ramp as necessary
        envelope = (isBeingTouched) ? 1 : 0;
        envelope2 = (isBeingTouched2) ? 1 : 0;
        
        // select waveform
        switch (waveform)
        {
            case 0:
                sample = (sin(theta)<0.98) * amplitude;             // rectangular wave
                sample2 = (sin(theta2)<0.98) * amplitude;
                break;
            case 1:
                sample = ((theta/M_PI)-1) * amplitude;              // sawtooth wave
                sample2 = ((theta2/M_PI)-1) * amplitude;
                break;
            case 2:
                sample = abs(((theta/M_PI)-1)) * 3 * amplitude;     // triangle wave
                sample2 = abs(((theta2/M_PI)-1)) * 3 * amplitude;
                break;
        }

        // add noise
        sample = sample + ((rand()%1000)/1000.0)*noiselevel;        
        sample2 = sample2 + ((rand()%1000)/1000.0)*noiselevel;
        
        // apply filter cascade, osc 1
        sample = LPFilter.filter(sample);                           // LPF
        sample = F1Filter.filter(sample);                           // Formant 1
        sample = F2Filter.filter(sample);                           // Formant 2
        sample = F3Filter.filter(sample);                           // Formant 3
        sample = F4Filter.filter(sample);                           // Formant 4
        
        // apply filter cascade, osc 2
        sample2 = LPFilter_2.filter(sample2);                           // LPF
        sample2 = F1Filter_2.filter(sample2);                           // Formant 1
        sample2 = F2Filter_2.filter(sample2);                           // Formant 2
        sample2 = F3Filter_2.filter(sample2);                           // Formant 3
        sample2 = F4Filter_2.filter(sample2);                           // Formant 4
        
        // apply envelopes, add, and write sample to buffer
        buffer[frame] = (sample * envelope) + (sample2 * envelope2 * 0.8);                          
        
        // increment and wrap phases
		theta += theta_increment;
        theta2 += theta_increment2;
		if (theta > 2.0 * M_PI)
			theta -= 2.0 * M_PI;
        if (theta2 > 2.0 * M_PI)
			theta2 -= 2.0 * M_PI;
    
	}
	
	// Store phases back in the view controller
	viewController->theta = theta;
    viewController->theta2 = theta2;

	return noErr;
}

// interruption listener. handle phone calls and whatnot here. 
void ToneInterruptionListener(void *inClientData, UInt32 inInterruptionState)
{
	/*ToneGeneratorViewController *viewController =
		(ToneGeneratorViewController *)inClientData;*/
}

@implementation ToneGeneratorViewController

@synthesize f0Slider;
@synthesize f0Title;
@synthesize f0Label;
@synthesize f0Field;
@synthesize f3Slider;
@synthesize f3Title;
@synthesize f3Label;
@synthesize f3Field;
@synthesize noiseSlider;
@synthesize noiseTitle;
@synthesize noiseLabel;
@synthesize f0Slider_2;
@synthesize f0Label_2;
@synthesize f0Field_2;
@synthesize f0Title_2;
@synthesize waveSelector;
@synthesize formantLabel;
@synthesize tiltSwitch;
@synthesize tiltTitle;
@synthesize pointerImage;
@synthesize pointerImage2;

#pragma mark - Audio methods

// starts audio
- (void)createToneUnit
{
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription defaultOutputDescription;
	defaultOutputDescription.componentType = kAudioUnitType_Output;
	defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	defaultOutputDescription.componentFlags = 0;
	defaultOutputDescription.componentFlagsMask = 0;
	
	// Get the default playback output unit
	AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
	NSAssert(defaultOutput, @"Can't find default output");
	
	// Create a new unit based on this that we'll use for output
	OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
	NSAssert1(toneUnit, @"Error creating unit: %d", err);
	
	// Set our tone rendering function on the unit
	AURenderCallbackStruct input;
	input.inputProc = RenderTone;
	input.inputProcRefCon = self;
	err = AudioUnitSetProperty(toneUnit, 
		kAudioUnitProperty_SetRenderCallback, 
		kAudioUnitScope_Input,
		0, 
		&input, 
		sizeof(input));
	NSAssert1(err == noErr, @"Error setting callback: %d", err);
	
	// Set the format to 32 bit, single channel, floating point, linear PCM
	const int four_bytes_per_float = 4;
	const int eight_bits_per_byte = 8;
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
		kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = four_bytes_per_float;
	streamFormat.mFramesPerPacket = 1;	
	streamFormat.mBytesPerFrame = four_bytes_per_float;		
	streamFormat.mChannelsPerFrame = 1;	
	streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
	err = AudioUnitSetProperty (toneUnit,
		kAudioUnitProperty_StreamFormat,
		kAudioUnitScope_Input,
		0,
		&streamFormat,
		sizeof(AudioStreamBasicDescription));
	NSAssert1(err == noErr, @"Error setting stream format: %d", err);
}

- (void)updateOsc:(int)osc x:(int)xpos y:(int)ypos {
    // mapping touch position to formants
    f1 = (1.875*(310-xpos))+300;
    f2 = (3.5*(450-ypos))+1000;
    
    // Refer to BQFilter.h for what these numbers mean.
    F1Filter.calc_filter_coeffs(6, f1, 44100.0, 1.0, 30.0, false);
    F2Filter.calc_filter_coeffs(6, f2, 44100.0, 1.0, 30.0, false);
    
    // update pointer location
    if (osc == 1)
        pointerImage.frame = CGRectMake(xpos-(pointerImage.frame.size.width/2), ypos-(pointerImage.frame.size.height/2), pointerImage.frame.size.width, pointerImage.frame.size.height);
    else
        pointerImage2.frame = CGRectMake(xpos-(pointerImage2.frame.size.width/2), ypos-(pointerImage2.frame.size.height/2), pointerImage2.frame.size.width, pointerImage2.frame.size.height); 
}

#pragma mark - UI methods

// slider listener
- (IBAction)sliderChanged:(id)sender
{
    UISlider *slider = (UISlider *) sender;
    switch (slider.tag)
    {
        case 1:
            f0 = slider.value;
            f0Field.text = [NSString stringWithFormat:@"%4.1f", f0];
            break;
        case 2:
            f3 = slider.value;
            F3Filter.calc_filter_coeffs(6, f3, 44100.0, 1.0, 30.0, false);
            f3Field.text = [NSString stringWithFormat:@"%4.0f", f3];
            break;
        case 3:
            noiselevel = 0.1 * slider.value;
            amplitude = 0.25 * (1.0 - slider.value);
            noiseLabel.text = [NSString stringWithFormat:@"%u%%", (int)(slider.value * 100.0)];
            break;
        case 4:
            f0_2 = slider.value;
            f0Field_2.text = [NSString stringWithFormat:@"%4.1f", f0_2];
            break;
    }
}

// selector listener
-(IBAction)selectorChanged:(id)sender
{
    waveform = [sender selectedSegmentIndex];
}

// button listener
-(IBAction)toggleChanged:(UISwitch*)sender
{
    isPaused = !sender.on;
}

// UITextFieldDelegate protocol method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * input = [f numberFromString:textField.text];
    [f release];

    if(input == nil) 
        textField.text = [NSString stringWithFormat:@"180.0"];
    else
    {
        double freq = [input doubleValue];
        // switch based on uitextfield's tag
        switch (textField.tag)
        {
            case 0:
                f0 = freq;
                f0Slider.value = freq;
                textField.text = [NSString stringWithFormat:@"%4.1f", freq];
                break;
            case 1:
                f3 = freq;
                F3Filter.calc_filter_coeffs(6, f3, 44100.0, 1.0, 30.0, false);
                f3Slider.value = freq;
                textField.text = [NSString stringWithFormat:@"%4.0f", freq];
                break;
            case 2:
                f0_2 = freq;
                f0Slider_2.value = freq;
                textField.text = [NSString stringWithFormat:@"%4.1f", freq];
                break;
        }
    }
    
    [textField resignFirstResponder];
    
    return YES;
}


// fades a view (used for the circle that follows the user's finger)
-(void)fade:(UIView*)viewToDissolve withDuration:(NSTimeInterval)duration   andWait:(NSTimeInterval)wait andType:(BOOL)type
{
    [UIView beginAnimations: @"Fade Out" context:nil];
    
    // wait for time before begin
    [UIView setAnimationDelay:wait];
    
    // duration of animation
    [UIView setAnimationDuration:duration];
    if (!type)
        viewToDissolve.alpha = 0.0;
    else
        viewToDissolve.alpha = 1.0;
    [UIView commitAnimations];
}

// Handles the start of a touch
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // set envelope params
    isBeingTouched = true;
    
    int xpos, ypos, xpos2, ypos2;
    
    // I should use NSSet touches from above. (multi touch is a little wonky atm)
    NSArray *t = [[event allTouches] allObjects];
//    bool multiTouch = ([t count] > 1) ? true : false;
    
    xpos = [[t objectAtIndex:0] locationInView:self.view].x;
    ypos = [[t objectAtIndex:0] locationInView:self.view].y;
    
    [self updateOsc:1 x:xpos y:ypos];
    
    if (!settings)
        [self fade : pointerImage withDuration: 1 andWait : 0 andType: true];   
    
//    // for multi touch, do the same thing again. i should factor out this code.
//    if (multiTouch)
//    {
//        isBeingTouched2 = true;
//        xpos2 = [[t objectAtIndex:1] locationInView:self.view].x;
//        ypos2 = [[t objectAtIndex:1] locationInView:self.view].y;
//        
//        [self updateOsc:2 x:xpos2 y:ypos2];
//        
//        if (!settings)
//            [self fade : pointerImage2 withDuration: 1 andWait : 0 andType: true];   
//    }
//    else
//        isBeingTouched2 = false;
    
    formantLabel.text = [NSString stringWithFormat:@"F0: %4.1f        F1: %4.1f        F2: %4.1f", f0, f1, f2];
}


// Handles the continuation of a touch.
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{  
    int xpos, ypos; //xpos2, ypos2;
    
    NSArray *t = [[event allTouches] allObjects];
//    bool multiTouch = ([t count] > 1) ? true : false;
    
    xpos = [[t objectAtIndex:0] locationInView:self.view].x;
    ypos = [[t objectAtIndex:0] locationInView:self.view].y;
    
    [self updateOsc:1 x:xpos y:ypos];
    
//    if (multiTouch)
//    {
//        isBeingTouched2 = true;
//        xpos2 = [[t objectAtIndex:1] locationInView:self.view].x;
//        ypos2 = [[t objectAtIndex:1] locationInView:self.view].y;
//        
//        [self updateOsc:2 x:xpos2 y:ypos2];
//        
//        if (!settings)
//            [self fade : pointerImage2 withDuration: 1 andWait : 0 andType: true]; 
//    }
//    else
//    {
//        if (!settings)
//            [self fade : pointerImage2 withDuration: 1 andWait : 0 andType: false];
//        
//        isBeingTouched2 = false;
//    }
    
    formantLabel.text = [NSString stringWithFormat:@"F0: %4.1f        F1: %4.1f        F2: %4.1f", f0, f1, f2];
}

// Handles the end of a touch event.
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    isBeingTouched = false;
//    isBeingTouched2 = false;
    
    if (!settings)
    {
        [self fade : pointerImage withDuration: 1 andWait : 0 andType: false];
//        [self fade : pointerImage2 withDuration: 1 andWait : 0 andType: false];
    }

    
}

// settings button listener (too lazy to make a settings view.)
- (void) settingsBtnPress:(id)sender{
    
    settings = (settings) ? false : true;
    
    //Add settings button
    UIBarButtonItem *settingsBtn = [[UIBarButtonItem alloc]initWithTitle:((settings)?@"   Back   ":@"Settings") style: UIBarButtonItemStyleBordered
                                                                  target: self 
                                                                  action: @selector(settingsBtnPress:)];
    //Add buttons to the array
    NSArray *items = [NSArray arrayWithObjects: settingsBtn, nil];
    //release buttons
    [settingsBtn release];
    //add buttons to toolbar
    [toolbar setItems:items animated:NO];
        
    [self fade : f0Slider withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Label withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Title withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Field withDuration: 1 andWait : 0 andType: settings];
    [self fade : f3Slider withDuration: 1 andWait : 0 andType: settings];
    [self fade : f3Label withDuration: 1 andWait : 0 andType: settings];
    [self fade : f3Field withDuration: 1 andWait : 0 andType: settings];
    [self fade : f3Title withDuration: 1 andWait : 0 andType: settings];
    [self fade : noiseSlider withDuration: 1 andWait : 0 andType: settings];
    [self fade : noiseLabel withDuration: 1 andWait : 0 andType: settings];
    [self fade : noiseTitle withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Slider_2 withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Label_2 withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Field_2 withDuration: 1 andWait : 0 andType: settings];
    [self fade : f0Title_2 withDuration: 1 andWait : 0 andType: settings];
    [self fade : waveSelector withDuration: 1 andWait : 0 andType: settings];
    [self fade : tiltSwitch withDuration: 1 andWait : 0 andType: settings];
    [self fade : tiltTitle withDuration: 1 andWait : 0 andType: settings];
    [self fade : background withDuration: 1 andWait : 0 andType: !settings];
    [self fade : formantLabel withDuration: 1 andWait : 0 andType: !settings];

}

#pragma mark - Accelerometer methods

// UIAccelerometerDelegate method, called when the device accelerates.
-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
	// Update the accelerometer
	if(!isPaused)
	{
		[filter addAcceleration:acceleration];
        f0 = (-acceleration.y+1.0)*70.0-20.0;
        if (f0 < 54.0)
            f0 = 54.0;
        [f0Slider setValue: f0];
        f0Field.text = [NSString stringWithFormat:@"%4.1f", f0];
        formantLabel.text = [NSString stringWithFormat:@"F0: %4.1f        F1: %4.1f        F2: %4.1f", f0, f1, f2];
	}
}

// accelerometer filter changer
-(void)changeFilter:(Class)filterClass
{
	// Ensure that the new filter class is different from the current one...
	if(filterClass != [filter class])
	{
		// And if it is, release the old one and create a new one.
		[filter release];
		filter = [[filterClass alloc] initWithSampleRate:kUpdateFrequency cutoffFrequency:5.0];
		// Set the adaptive flag
		filter.adaptive = useAdaptive;
	}
}

#pragma mark - ViewController lifecycle

- (void)dealloc {
    [toolbar release];
    [f0Slider release];
    [f0Field release];
    [f3Slider release];
    [f3Field release];
    [noiseSlider release];
    [f0Slider_2 release];
    [f0Field_2 release];
    [waveSelector release];
    [super dealloc];
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
    // accelerometer initialization
    isPaused = NO;
	useAdaptive = NO;
	[self changeFilter:[LowpassFilter class]];
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0 / kUpdateFrequency];
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
    
    // seed RNG
    srand ( time(NULL) );
    
    // UI Initialization
    // add background
    background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vowels.png"]];
    [self.view addSubview:background];
    [background release];
    
    // create toolbar
    toolbar = [UIToolbar new];
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    [toolbar sizeToFit];
    toolbar.frame = CGRectMake(0, 410, 320, 50);
    // Add buttons
    UIBarButtonItem *settingsBtn = [[UIBarButtonItem alloc]initWithTitle:@"Settings" style: UIBarButtonItemStyleBordered
                                                                  target: self 
                                                                  action: @selector(settingsBtnPress:)];
    // Add buttons to the array
    NSArray *items = [NSArray arrayWithObjects: settingsBtn, nil];
    // release buttons
    [settingsBtn release];
    // add array of buttons to toolbar
    [toolbar setItems:items animated:NO];
    // add toolbar to view
    [self.view addSubview:toolbar];
    
    // don't display settings
    settings = false;
    
    // initialize slider values, set tags
    [f0Slider setValue:120];
    [f0Slider setTag: 1];
	[self sliderChanged:f0Slider];
    
    [f3Slider setValue:3500.0];
    [f3Slider setTag: 2];
	[self sliderChanged:f3Slider];
    
    [noiseSlider setValue:0.5];
    [noiseSlider setTag: 3];
	[self sliderChanged:noiseSlider];
    
    [f0Slider_2 setValue:180];
    [f0Slider_2 setTag: 4];
	[self sliderChanged:f0Slider_2];
    
    // initialize text fields
    f0Field.delegate = self;
    f3Field.delegate = self;
    f0Field_2.delegate = self;
    
    // initialize sample rate, waveform
	sampleRate = 44100;
    waveform = 0;
    isPaused = YES;
    
    // set settings controls invisible
    f0Slider.alpha = 0;
    f0Label.alpha = 0;
    f0Title.alpha = 0;
    f0Field.alpha = 0;
    f3Slider.alpha = 0;
    f3Label.alpha = 0;
    f3Field.alpha = 0;
    f3Title.alpha = 0;
    noiseSlider.alpha = 0;
    noiseLabel.alpha = 0;
    noiseTitle.alpha = 0;
    f0Slider_2.alpha = 0;
    f0Label_2.alpha = 0;
    f0Field_2.alpha = 0;
    f0Title_2.alpha = 0;
    waveSelector.alpha = 0;
    tiltSwitch.alpha = 0;
    tiltTitle.alpha = 0;
    pointerImage.alpha = 0;
    pointerImage2.alpha = 0;
    background.alpha = 1;
    formantLabel.alpha = 1;

    // AUDIO INITIALIZATION
    // static LPF, F3, F4 coefficients
    LPFilter.calc_filter_coeffs(0, 250.0, 44100.0, 0.5, 100.0, false);
    F3Filter.calc_filter_coeffs(6, 2500.0, 44100.0, 1.0, 30.0, false);
    F4Filter.calc_filter_coeffs(6, 3000.0, 44100.0, 0.5, 20.0, false);
    LPFilter_2.calc_filter_coeffs(0, 250.0, 44100.0, 0.5, 100.0, false);
    F3Filter_2.calc_filter_coeffs(6, 2500.0, 44100.0, 1.0, 30.0, false);
    F4Filter_2.calc_filter_coeffs(6, 3000.0, 44100.0, 0.5, 20.0, false);
    
	OSStatus result = AudioSessionInitialize(NULL, NULL, ToneInterruptionListener, self);
	if (result == kAudioSessionNoError)
	{
		UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	}
    
    // initialize audio
	AudioSessionSetActive(true);
    [self createToneUnit];
    OSErr err = AudioUnitInitialize(toneUnit);
    NSAssert1(err == noErr, @"Error initializing unit: %d", err);
    
    // Start and stop to initialize (kind of a hack)
    err = AudioOutputUnitStart(toneUnit);
    NSAssert1(err == noErr, @"Error starting unit: %d", err);
    AudioOutputUnitStop(toneUnit);
    AudioUnitUninitialize(toneUnit);
    AudioComponentInstanceDispose(toneUnit);
    toneUnit = nil;
    
    // start tone unit
    [self createToneUnit];
    
    // Stop changing parameters on the unit
    err = AudioUnitInitialize(toneUnit);
    NSAssert1(err == noErr, @"Error initializing unit: %d", err);
    
    // Start playback
    err = AudioOutputUnitStart(toneUnit);
    NSAssert1(err == noErr, @"Error starting unit: %d", err);
    
    // set touch
    isBeingTouched = false;
    isBeingTouched2 = false;
    
    
    // Create a banner view of the standard size at the bottom of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = @"a150097c09c3bf1";
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];
    
    GADRequest *request = [GADRequest request];
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:request];
    
}

- (void)viewDidUnload {
	self.f0Label = nil;
	self.f0Slider = nil;
    self.f0Title = nil;
    self.f0Field = nil;
    self.f3Label = nil;
    self.f3Title = nil;
	self.f3Slider = nil;
    self.f3Field = nil;
    self.noiseLabel = nil;
	self.noiseSlider = nil;
    self.noiseTitle = nil;
    self.f0Slider_2 = nil;
    self.f0Label_2 = nil;
    self.f0Field_2 = nil;
    self.f0Title_2 = nil;
    self.tiltTitle = nil;
    self.waveSelector = nil;
    self.formantLabel = nil;
    
    AudioOutputUnitStop(toneUnit);
    AudioUnitUninitialize(toneUnit);
    AudioComponentInstanceDispose(toneUnit);
    toneUnit = nil;
    
	AudioSessionSetActive(false);
    
    [bannerView_ release];
}

@end