//
//  VSAppDelegate.h
//  VowelSynth
//
//  Ben Guo 2011
//
//  boilerplate code for audio initialization from Matt Gallagher's tutorial
//  http://cocoawithlove.com/2010/10/ios-tone-generator-introduction-to.html
//  Copyright 2010 Matt Gallagher. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ToneGeneratorViewController;

@interface VSAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ToneGeneratorViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ToneGeneratorViewController *viewController;

@end

