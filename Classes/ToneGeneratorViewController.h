//
//  ToneGeneratorViewController.h
//  VowelSynth
//
//  Ben Guo 2011
//
//  boilerplate code for audio initialization from Matt Gallagher's tutorial
//  http://cocoawithlove.com/2010/10/ios-tone-generator-introduction-to.html
//  Copyright 2010 Matt Gallagher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioUnit/AudioUnit.h>
#import "GADBannerView.h"
#include "BQFilter.h"

@class AccelerometerFilter;

@interface ToneGeneratorViewController : UIViewController<UIAccelerometerDelegate, UITextFieldDelegate>
{
	UILabel *f0Label;
    UILabel *f0Title;
	UISlider *f0Slider; 
    UITextField *f0Field;
    UILabel *f3Label;
    UILabel *f3Title;
    UITextField *f3Field;
	UISlider *f3Slider;    
    UILabel *noiseLabel;
    UILabel *noiseTitle;
	UISlider *noiseSlider;
    UISlider *f0Slider_2;
    UILabel *f0Label_2;
    UITextField *f0Field_2;
    UILabel *f0Title_2;
    UILabel *formantLabel;
    UISegmentedControl *waveSelector;
    UILabel *tiltTitle;
    UISwitch *tiltSwitch;
    UIImageView *pointerImage;
    UIImageView *pointerImage2;
    UIImageView *background;
    
    UIToolbar *toolbar;
    
	AudioComponentInstance toneUnit;
    
    AccelerometerFilter *filter;
    CGPoint startTouchPosition;
    
    GADBannerView *bannerView_;
    
@public
	double f0, f0_2;
    double f3;
    double f1, f2, f1_2, f2_2;
    double amplitude;
    double noiselevel;
	double sampleRate;
	double theta, theta2;
    bool isBeingTouched;    // TRUE if screen is being touched
    bool isBeingTouched2; 
    int waveform;           // 0 = pulse, 1 = saw, 2 = glottal pulse
    BOOL settings;
    BOOL isPaused, useAdaptive;
}

@property (nonatomic, retain) IBOutlet UISlider *f0Slider;
@property (nonatomic, retain) IBOutlet UILabel *f0Title;
@property (nonatomic, retain) IBOutlet UILabel *f0Label;
@property (nonatomic, retain) IBOutlet UITextField *f0Field;
@property (nonatomic, retain) IBOutlet UISlider *f3Slider;
@property (nonatomic, retain) IBOutlet UILabel *f3Title;
@property (nonatomic, retain) IBOutlet UITextField *f3Field;
@property (nonatomic, retain) IBOutlet UILabel *f3Label;
@property (nonatomic, retain) IBOutlet UISlider *noiseSlider;
@property (nonatomic, retain) IBOutlet UILabel *noiseTitle;
@property (nonatomic, retain) IBOutlet UILabel *noiseLabel;
@property (nonatomic, retain) IBOutlet UISlider *f0Slider_2;
@property (nonatomic, retain) IBOutlet UILabel *f0Title_2;
@property (nonatomic, retain) IBOutlet UILabel *f0Label_2;
@property (nonatomic, retain) IBOutlet UITextField *f0Field_2;
@property (nonatomic, retain) IBOutlet UILabel *formantLabel;
@property (nonatomic, retain) IBOutlet UISegmentedControl *waveSelector;
@property (nonatomic, retain) IBOutlet UISwitch *tiltSwitch;
@property (nonatomic, retain) IBOutlet UILabel *tiltTitle;
@property (nonatomic, retain) IBOutlet UIImageView *pointerImage;
@property (nonatomic, retain) IBOutlet UIImageView *pointerImage2;

- (IBAction)sliderChanged:(id)sender;
- (IBAction)selectorChanged:(id)sender;
- (IBAction)toggleChanged:(id)sender;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

@end

